#!/usr/bin/env guile
--no-debug
!#

;; init PRNG
(set! *random-state* (random-state-from-platform))

;; prefixes (list of symbols)
(define prefixes '(
                   Pre Ini Anti Contra Con Com
                   Pro Ge Tele Proxy Trans
                   De Dis En Em Fore In Im
                   Inter Mid Mis Non Over
                   Re Semi Sub Super Un Under
                   Homo Bene Dia Micro Mini Nano
                   Macro Hyper Multi Para Poly
                   Post Retro Thermo Uni Duo Ex
                   Mono Di Tri Tetra Penta Hexa
                   Hepto Octo Deca Hemi Kilo
                   Giga Mega Peta
                   Gyno Andro Feminae
                   Auto Circo Co Ex Extra Hetero
                   Immo Irre Ille Inco
                   Intra Omni Syn A Magne Epi
                   ))

;; suffixes (list of symbols)
(define suffixes '(
                   ter der zer ser est por
                   erk mor tor teur ness
                   able ible al ial
                   ed ted ked red
                   en ten ken ren
                   ful lic nic
                   ing ling sing ting
                   ion sion tion
                   ity ty tive
                   ify fy ize ise nce
                   dom lism nism kism tism
                   sism rism mism vism xism
                   hood logy ry ish
                   less ly ment s ss ng
                   lous vous teous kious
                   chrom chron meter tron
                   phobia rupt spect scope
                   phone port drome dron lab
                   genesis phytes sperm mixis
                   ))

;; all the latin single letters (list of characters)
(define letters
  (map (lambda (x) (integer->char (+ 97 x))) (iota 26)))

;; vowel sounds (list of strings)
(define vowels
  (map symbol->string '( a e i o u yu ye ae eu aue ee
                           a e i o u a e o e)))

;; consonant sounds (list of strings)
(define consonants
  (append
   (map string
        (filter (lambda (x)
                  (not (member (string x) vowels))) letters))
   (map symbol->string '(th ck br pr tr pl cr nc ss))
   '(m n t r s p v b m n p r t l k m n p r t)))


;; FIXME: just to play around
(define test-it
  (lambda (x)
    (newline)(display "testing: ")(display x)(newline)
    (display "is char? ")(display (char? x))
    (newline)
    (display "is string? ")(display (string? x))
    (newline)
    (display "is symbol? ")(display (symbol? x))
    (newline)
    ))

;;--------------------------------------------------
;; generating random sequence prefix-mimimi-suffix

(display (list-ref prefixes (random (length prefixes))))

(display (list-ref consonants (random (length consonants))))
(display (list-ref vowels (random (length vowels))))
(display (list-ref consonants (random (length consonants))))
(display (list-ref vowels (random (length vowels))))

(display (list-ref suffixes (random (length suffixes))))

;;(newline)
;;(test-it (car vowels))
;;(display (filter (lambda (x) (not (consonant x))) letters))
;;(display consonants)
;;(test-it (car consonants))
(newline)
