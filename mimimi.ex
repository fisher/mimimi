defmodule Mimimi do

  def start do
    :io.format "~s~n", [get_string()]
  end

  # main mean interface
  def get_string do
    raw_string() |> IO.iodata_to_binary
  end

  # for time measurement
  def get_string(howmany) do
    for _ <- :lists.seq(1,howmany), do: get_string()
  end

  # just for playing around
  def raw_string do
    # add some double sounds
    consonants = consonants() ++ ["tr", "dr", "pr", "ts"]
    # triple the probability for a regular sound than diphthong
    vowels =
      :lists.foldl(
        fn(e,acc) ->
          [e,e,e|acc]
        end,
        vowelsounds(),
        vowels())
    # form the raw mimimi
    pref = prefixes()
    suff = suffixes()
    [ elem(pref, :rand.uniform(tuple_size(pref))-1),
      get_mimimi(consonants, vowels),
      elem(suff, :rand.uniform(tuple_size(suff))-1) ]
  end

## --------------------------------------------------
## local funs
## --------------------------------------------------

  # entry /2
  defp get_mimimi(c, v) do
    get_mimimi(c, v, :rand.uniform(3), [])
  end

  # aux /4
  defp get_mimimi(_, _, 0, acc) do
    acc
  end

  defp get_mimimi(c, v, i, acc) do
    new_acc = [get_rnd(c), get_rnd(v) |acc]
    get_mimimi(c, v, i-1, new_acc)
  end

  defp consonants() do
    vowels = vowels() ++ ["j", "q", "w", "x", "y", "z"]
    letters = for i <- :lists.seq(97, 96 +26), do: <<i>>
    letters -- vowels
  end

  defp vowels() do
    ["a", "e", "i", "o", "u"]
  end

  # extra vowel sounds (diphthongs, long sounds)
  defp vowelsounds() do
    ["ae", "ee", "oi", "ai"]
  end

  defp get_rnd(list) do
    :lists.nth(:rand.uniform(length(list)), list)
  end

  # common word prefixes
  defp prefixes() do
    {
     "Pre", "Ini", "Anti", "Contra", "Con", "Com", "Pro",
     "Ge", "Tele", "Proxy", "Trans", "De", "Dis", "En", "Em", "Fore", "In",
     "Im", "Inter", "Mid", "Mis", "Non", "Over", "Re", "Semi", "Sub",
     "Super", "Un", "Under", "Homo", "Bene", "Dia", "Micro", "Mini",
     "Nano", "Macro", "Hyper", "Multi", "Para", "Poly", "Post", "Retro",
     "Thermo", "Uni", "Duo", "Ex", "Mono", "Di", "Tri", "Tetra", "Penta",
     "Hexa", "Hepto", "Octo", "Deca", "Hemi", "Kilo", "Giga", "Mega",
     "Peta", "Gyno", "Andro", "Feminae", "Auto", "Circo", "Co", "Ex",
     "Extra", "Hetero", "Immo", "Irre", "Ille", "Inco", "Intra", "Omni",
     "Syn", "A", "Magne", "Epi"
    }
  end

  defp suffixes() do
    {
     "ter", "der", "zer", "ser", "est", "por", "erk", "mor",
     "tor", "teur", "ness", "able", "ible", "al", "ial", "ed", "ted",
     "ked", "red", "en", "ten", "ken", "ren", "ful", "lic", "nic", "ing",
     "ling", "sing", "ting", "ion", "sion", "tion", "ity", "ty", "tive",
     "ify", "fy", "ize", "ise", "nce", "dom", "lism", "nism", "kism",
     "tism", "sism", "rism", "mism", "vism", "xism", "hood", "logy", "ry",
     "ish", "less", "ly", "ment", "s", "ss", "ng", "lous", "vous", "teous",
     "kious", "chrom", "chron", "meter", "tron", "phobia", "rupt", "spect",
     "scope", "phone", "port", "drome", "dron", "lab", "genesis", "phytes",
     "sperm", "mixis"
    }
  end

end
