(* how the hell can we create a list? *)

(* integer constant negation is ~1, not -1 *)

val list = [ 2,3,4,5];

val prefixes = [
    "Pre", "Ini", "Anti", "Contra", "Con", "Com", "Pro",
    "Ge", "Tele", "Proxy", "Trans", "De", "Dis", "En", "Em", "Fore", "In",
    "Im", "Inter", "Mid", "Mis", "Non", "Over", "Re", "Semi", "Sub",
    "Super", "Un", "Under", "Homo", "Bene", "Dia", "Micro", "Mini",
    "Nano", "Macro", "Hyper", "Multi", "Para", "Poly", "Post", "Retro",
    "Thermo", "Uni", "Duo", "Ex", "Mono", "Di", "Tri", "Tetra", "Penta",
    "Hexa", "Hepto", "Octo", "Deca", "Hemi", "Kilo", "Giga", "Mega",
    "Peta", "Gyno", "Andro", "Feminae", "Auto", "Circo", "Co", "Ex",
    "Extra", "Hetero", "Immo", "Irre", "Ille", "Inco", "Intra", "Omni",
    "Syn", "A", "Magne", "Epi"
];

val suffixes =
    [
      "ter", "der", "zer", "ser", "est", "por", "erk", "mor",
      "tor", "teur", "ness", "able", "ible", "al", "ial", "ed", "ted",
      "ked", "red", "en", "ten", "ken", "ren", "ful", "lic", "nic", "ing",
      "ling", "sing", "ting", "ion", "sion", "tion", "ity", "ty", "tive",
      "ify", "fy", "ize", "ise", "nce", "dom", "lism", "nism", "kism",
      "tism", "sism", "rism", "mism", "vism", "xism", "hood", "logy", "ry",
      "ish", "less", "ly", "ment", "s", "ss", "ng", "lous", "vous", "teous",
      "kious", "chrom", "chron", "meter", "tron", "phobia", "rupt", "spect",
      "scope", "phone", "port", "drome", "dron", "lab", "genesis", "phytes",
      "sperm", "mixis"
    ];

fun sum([]) = 0 |
    sum(a::rest) = a + sum(rest);

fun repeat([]) = [] |
    repeat(a::rest) = a::a::repeat(rest);

(* val nextInt = Random.randRange (1,100);
- val r = Random.rand (1,1);
- val x1 = nextInt r;
- val x2 = nextInt r; *)
