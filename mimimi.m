%% mimimi.m -- matlab version

% random seed; rng() exists only in newest versions of matlab
if exist('rng')
    rng('shuffle');
end

% apply char() to a vector of ASCII codes of a..z
letters = char(97:122);

% vector of vowel characters
vowels = ['a' 'e' 'i' 'u' 'o'];

% filter using ismember() predicate; it creates a vector of booleans and
% just like in R apply it to the original vector to filter out the elements 
consonants = letters(~ismember(letters, vowels));

%% using cell arrays for strings with variable length
% also, matlab is stupid enough and does not have a list structure.
% so, the next two defined objects are matrices with 7 columns
prefix = {
           'Pre' 'Ini' 'Anti' 'Contra' 'Con' 'Com' 'Pro'
           'Ge' 'Tele' 'Proxy' 'Trans' 'De' 'Dis' 'En'
           'Em' 'Fore' 'In' 'Im' 'Inter' 'Mid' 'Mis'
           'Non' 'Over' 'Re' 'Semi' 'Sub' 'Super' 'Un'
           'Under' 'Homo' 'Bene' 'Dia' 'Micro' 'Mini' 'Nano'
           'Macro' 'Hyper' 'Multi' 'Para' 'Poly' 'Post' 'Retro'
           'Thermo' 'Uni' 'Duo' 'Ex' 'Mono' 'Di' 'Tri'
           'Tetra' 'Penta' 'Hexa' 'Hepto' 'Octo' 'Deca' 'Hemi'
           'Kilo' 'Giga' 'Mega' 'Peta' 'Gyno' 'Andro' 'Feminae'
           'Auto' 'Circo' 'Co' 'Ex' 'Extra' 'Hetero' 'Immo'
           'Irre' 'Ille' 'Inco' 'Intra' 'Omni' 'Syn' 'A'
           'Magne' 'Epi' 'Bi' 'Di' 'De' 'Duo' 'The'
         };

suffix = {
          'ter' 'der' 'zer' 'ser' 'est' 'por' 'erk'
          'mor' 'tor' 'teur' 'ness' 'able' 'ible' 'al'
          'ial' 'ed' 'ted' 'ked' 'red' 'en' 'ten'
          'ken' 'ren' 'ful' 'lic' 'nic' 'ing' 'ling'
          'sing' 'ting' 'ion' 'sion' 'tion' 'ity' 'ty'
          'tive' 'ify' 'fy' 'ize' 'ise' 'nce' 'dom'
          'lism' 'nism' 'kism' 'tism' 'sism' 'rism' 'mism'
          'vism' 'xism' 'hood' 'logy' 'ry' 'ish' 'less'
          'ly' 'ment' 's' 'ss' 'ng' 'lous' 'vous'
          'teous' 'kious' 'chrom' 'chron' 'meter' 'tron' 'phobia'
          'rupt' 'spect' 'scope' 'phone' 'port' 'drome' 'dron'
          'lab' 'genesis' 'phytes' 'sperm' 'mixis' 'lly' 'sis'
         };

% select one element out of matrix by random integer index taken from
% the range from 1 to the product of size vector of matrix object.
% init & tail will be the cell array ref, and it needs stvcat() or char()
init = prefix(randi(prod(size(prefix)), 1,1));
tail = suffix(randi(numel(suffix), 1,1));

%% creating matrix with row of 3 consonants and row of 3 vowels
% since matrix arranged just like in fortran, column-wise, -- when printed,
% it prints first consonant [1,1], then vowel [2,1], then consonant [1,2]
% and so on up to the last vowel [2,3]
mimi = [ consonants(randi(size(consonants), 1,3));
         vowels(randi(size(vowels), 1, 3)) ];

%% print the final result
fprintf('%s%s%s\n', char(init), mimi, char(tail));
