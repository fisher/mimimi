#!/usr/bin/env perl
#

use strict;
use warnings;

# maximum number of random syllables in the middle (1..n)
my $maxmimimi = 3;

my @prefixes = qw{
                   Pre Ini Anti Contra Con Com
                   Pro Ge Tele Proxy Trans
                   De Dis En Em Fore In Im
                   Inter Mid Mis Non Over
                   Re Semi Sub Super Un Under
                   Homo Bene Dia Micro Mini Nano
                   Macro Hyper Multi Para Poly
                   Post Retro Thermo Uni Duo Ex
                   Mono Di Tri Tetra Penta Hexa
                   Hepto Octo Deca Hemi Kilo
                   Giga Mega Peta
                   Gyno Andro Feminae
                   Auto Circo Co Ex Extra Hetero
                   Immo Irre Ille Inco
                   Intra Omni Syn A Magne Epi
               };

my @suffixes = qw{
                   ter der zer ser est por
                   erk mor tor teur ness
                   able ible al ial
                   ed ted ked red ged
                   en ten ken ren gen
                   ful lic nic
                   ing ling sing ting
                   ion sion tion
                   ity ty tive
                   ify fy ize ise nce
                   dom lism nism kism tism
                   sism rism mism vism xism
                   hood logy ry ish
                   less ly ment s ss ng
                   lous vous teous kious
                   chrom chron meter tron
                   phobia rupt spect scope
                   phone port drome dron lab
                   genesis phytes sperm mixis
               };

# composite suffix tails, starting from consonant
my @cstail = qw{
                 logy tion sion zion zzy zy
                 ness less fess son sson dom
                 ment ship ward wise ly
             };

# composite suffix tails, starting from vowel
my @vstail = qw{
                 er or erk al ial ed
                 ible able eble ive
                 est iest ist aist oist
                 ing eng en es ess ic ul ical
                 ify ivy ize ise ism om ion
                 ish esh ous eous ious
                 icious itious ivious
                 acy ance ence esque y
             };

# composite suffix heads, to close open vowel
my @vshead = qw{
                 t r s f g n w p l k h d z c b m
                 ss br pr tr pl cr nc
             };


my @letters = ("a".."z");
my @vowels = qw/a e i o u y ae eu aue ee/;
my @consonants = grep { $a=$_; ! grep /$a/, @vowels } @letters;

my $prefix = $prefixes[rand(scalar @prefixes)];

my $suffix;

if (rand() >0.68) {
  $suffix = $suffixes[rand(scalar @suffixes)];
} elsif (rand() > .5) {
  $suffix = $vshead[rand(scalar @vshead)].$vstail[rand(scalar @vstail)];
} else {
  $suffix = $cstail[rand(scalar @cstail)];
}

my $mimimi;

foreach (1..int(rand($maxmimimi) +1)) {
  $mimimi .= $consonants[rand(scalar @consonants)].
    $vowels[rand(scalar @vowels -1)];
}

my $name = $prefix.$mimimi.$suffix;

$name =~ s/ii/i/g;
$name =~ s/sz/s/g;

print $name."\n";

