#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char *prefixes[] = {
    "Pre", "Ini", "Anti", "Contra", "Con", "Com", "Pro",
    "Ge", "Tele", "Proxy", "Trans", "De", "Dis", "En", "Em", "Fore", "In",
    "Im", "Inter", "Mid", "Mis", "Non", "Over", "Re", "Semi", "Sub",
    "Super", "Un", "Under", "Homo", "Bene", "Dia", "Micro", "Mini",
    "Nano", "Macro", "Hyper", "Multi", "Para", "Poly", "Post", "Retro",
    "Thermo", "Uni", "Duo", "Ex", "Mono", "Di", "Tri", "Tetra", "Penta",
    "Hexa", "Hepto", "Octo", "Deca", "Hemi", "Kilo", "Giga", "Mega",
    "Peta", "Gyno", "Andro", "Feminae", "Auto", "Circo", "Co", "Ex",
    "Extra", "Hetero", "Immo", "Irre", "Ille", "Inco", "Intra", "Omni",
    "Syn", "A", "Magne", "Epi"
};

char *suffixes[] = {
    "ter", "der", "zer", "ser", "est", "por", "erk", "mor",
    "tor", "teur", "ness", "able", "ible", "al", "ial", "ed", "ted",
    "ked", "red", "en", "ten", "ken", "ren", "ful", "lic", "nic", "ing",
    "ling", "sing", "ting", "ion", "sion", "tion", "ity", "ty", "tive",
    "ify", "fy", "ize", "ise", "nce", "dom", "lism", "nism", "kism",
    "tism", "sism", "rism", "mism", "vism", "xism", "hood", "logy", "ry",
    "ish", "less", "ly", "ment", "s", "ss", "ng", "lous", "vous", "teous",
    "kious", "chrom", "chron", "meter", "tron", "phobia", "rupt", "spect",
    "scope", "phone", "port", "drome", "dron", "lab", "genesis", "phytes",
    "sperm", "mixis", "gen"
};

char letters[26];

char vowels[] = { 'a', 'e', 'i', 'u', 'o' };

char consonants[26-sizeof(vowels)];

int is_vowel(char letter) {
    unsigned i;

    for (i=0; i<sizeof(vowels); i++) {
        if (letter == vowels[i]) return 1;
    }
    return 0;
}

int main() {

    /* I declare it first because I'm lazy enough to change the --std
       in makefile */
    int i,consonant_cnt;

    /* init the PRNG */
    srand(time(NULL));

    /* create an array of all latin letters */
    for (i=0; i<26; i++) letters[i] = i +97;

    /* and now an array of just consonants */
    for (i=0,consonant_cnt=0; i<26; i++) {
        if (! is_vowel(letters[i])) consonants[consonant_cnt++] = letters[i];
    }

    printf("%s%c%c%c%c%s\n",
           prefixes[rand() % (sizeof(prefixes) /sizeof(char*))],
           consonants[rand() % consonant_cnt],
           vowels[rand() % sizeof(vowels)],
           consonants[rand() % consonant_cnt],
           vowels[rand() % sizeof(vowels)],
           suffixes[rand() % (sizeof(suffixes) /sizeof(char*))]);

    return 0;
}
