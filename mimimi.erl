-module(mimimi).

-export([get_string/0, raw_string/0, start/0]).

-type ascii() :: Letter::97..122.
-type sound() :: nonempty_list(ascii()).

%% just print it
-spec start() -> ok.
start() ->
    io:format("~s~n", [get_string()]).

%% main mean interface
-spec get_string() -> String :: list(ascii()).
get_string() ->
    lists:flatten(raw_string()).

%% just for playing around
-spec raw_string() -> io_lib:chars().
raw_string() ->
    %% add some double sounds
    Consonants = [[E] || E<-consonants()] ++ ["tr", "dr", "pr"],
    %% triple the probability for a regular sound than diphthong
    Vowels =
        lists:foldl(
          fun(E,Acc) ->
                  [[E],[E],[E]|Acc]
          end,
          vowelsounds(),
          vowels()),
    %% form the raw mimimi
    [ get_rnd(prefixes()),
      get_mimimi(Consonants, Vowels),
      get_rnd(suffixes()) ].

%% --------------------------------------------------
%% local funs
%% --------------------------------------------------

-spec get_mimimi(
        Consonants::list(sound()),
        Vowels::list(sound())) ->
                        Mimimi::list(sound()).
get_mimimi(C, V) ->
    get_mimimi(C, V, rand:uniform(3), []).

get_mimimi(_, _, 0, Acc) ->
    Acc;
get_mimimi(C, V, I, Acc) ->
    get_mimimi(C, V, I -1, [get_rnd(C), get_rnd(V) |Acc]).

-spec consonants() -> list(sound()).
consonants() ->
    Vowels = vowels() ++ "jqwxyz", %% I dislike the combinations "jae"/"qoi"
    Filter = fun(Elem) -> not lists:member(Elem, Vowels) end,
    lists:filter(Filter, lists:seq(97, 96 +26)).

vowels() ->
    "aeiou".

%% extra vowel sounds (diphthongs, long sounds)
vowelsounds() ->
    ["ae", "ee", "oi", "ai"].

get_rnd(List) ->
    lists:nth(rand:uniform(length(List)), List).

%% common word prefixes
prefixes() ->
    [
     "Pre", "Ini", "Anti", "Contra", "Con", "Com", "Pro",
     "Ge", "Tele", "Proxy", "Trans", "De", "Dis", "En", "Em", "Fore", "In",
     "Im", "Inter", "Mid", "Mis", "Non", "Over", "Re", "Semi", "Sub",
     "Super", "Un", "Under", "Homo", "Bene", "Dia", "Micro", "Mini",
     "Nano", "Macro", "Hyper", "Multi", "Para", "Poly", "Post", "Retro",
     "Thermo", "Uni", "Duo", "Ex", "Mono", "Di", "Tri", "Tetra", "Penta",
     "Hexa", "Hepto", "Octo", "Deca", "Hemi", "Kilo", "Giga", "Mega",
     "Peta", "Gyno", "Andro", "Feminae", "Auto", "Circo", "Co", "Ex",
     "Extra", "Hetero", "Immo", "Irre", "Ille", "Inco", "Intra", "Omni",
     "Syn", "A", "Magne", "Epi"
    ].

suffixes() ->
    [
     "ter", "der", "zer", "ser", "est", "por", "erk", "mor",
     "tor", "teur", "ness", "able", "ible", "al", "ial", "ed", "ted",
     "ked", "red", "en", "ten", "ken", "ren", "ful", "lic", "nic", "ing",
     "ling", "sing", "ting", "ion", "sion", "tion", "ity", "ty", "tive",
     "ify", "fy", "ize", "ise", "nce", "dom", "lism", "nism", "kism",
     "tism", "sism", "rism", "mism", "vism", "xism", "hood", "logy", "ry",
     "ish", "less", "ly", "ment", "s", "ss", "ng", "lous", "vous", "teous",
     "kious", "chrom", "chron", "meter", "tron", "phobia", "rupt", "spect",
     "scope", "phone", "port", "drome", "dron", "lab", "genesis", "phytes",
     "sperm", "mixis"
    ].

