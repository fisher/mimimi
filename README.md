# mimimi

creating random names from random syllables

# usage

Simple as in

```
0 (mimimi:master) sofa% ./mimimi.scm
Diplupess
0 (mimimi:master) sofa% ./mimimi.pl
Amopuposon
0 (mimimi:master) sofa% ./mimimi.R
Octopowaty
```

# purpose

In fact this repo is just a sandbox for various programming languages
for me to try. The simple algo should be implemented for all the
instances - first create a list of consonants and list of vowels, then
combine it randomly in a loop like 'mimimi' (hence the name), and
print the result string. It is a simple task and it should be good for
learning purposes.

Some of the programs here can be more sophisticated, though =)
