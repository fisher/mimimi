
CC := gcc
#CC := tcc

ERLC := $(shell which erlc)

.PHONY: clean

usage:
	@echo "run 'make mimimi_c' or 'make mimimi_erl'"

all: mimimi_c mimimi_erl

mimimi_c: mimimi.c
	$(CC) -o mimimi_c mimimi.c

mimimi.beam: mimimi.erl
	@[ -n "$(ERLC)" ] || echo "No erlang compiler in your \$$PATH"
	@[ -n "$(ERLC)" ]
	$(ERLC) mimimi.erl

mimimi_erl: mimimi.beam
	echo "#!/bin/sh" > mimimi_erl
	echo "erl -noinput -noshell -pa . -s mimimi -s erlang halt" >> mimimi_erl
	chmod a+x mimimi_erl

Elixir.Mimimi.beam: mimimi.ex
	elixirc mimimi.ex

mimimi_elixir: Elixir.Mimimi.beam
	echo "run"

clean:
	rm -f mimimi_c mimimi.beam mimimi_erl
